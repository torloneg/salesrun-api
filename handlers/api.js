// ************************************************************
// SALES RUN
// ************************************************************
// 2014-2017
// TORLONE GIANFRANCO
// ************************************************************

var deleteKey = require('key-del');
var Boom = require('boom');
var _ = require('underscore');
var moment = require('moment');

function getFields( table_name, evento_data, evento_agente_uuid, evento_uuid, lista_campi,fields) {



}


exports.addEvent  = function ( request,reply) {

	const Config  = request.server.settings.app;
	const clienti = Config.clienti;
	const tabelle = Config.tabelle;


	var cliente_uuid    = request.params.uuid_cliente;
	var collection_name = request.params.collection;

  var cliente = _.first(_.where(clienti, {uuid: cliente_uuid}));


	if ( cliente == null  ) {
		reply(Boom.notFound('cliente non presente'));
	} else {

		var table_name = "eventi_"+ cliente.idcliente +"_"+collection_name;
		//console.log(table_name);

		var lista_campi = _.where(tabelle, {"table_name":table_name})
		//console.log(lista_campi);
		if ( lista_campi.length==0 ) {
			reply(Boom.notFound('la collection non esiste'));
		} else {

				var element = request.payload.fields;
				element["evento_data"] = request.payload.evento_data;
				element["evento_agente_uuid"] = request.payload.evento_agente_uuid;
				element["evento_uuid"] = request.payload.evento_uuid;
				element["evento_timestamp"] = moment().format('YYYY-MM-DD HH:mm:ss');


				request.server.settings.app.knexEventi.insert(element, 'evento_id').into(table_name).then(function() {
					reply({result:'OK'})
				}).catch(function(error) {

				  request.server.logger().error({cliente_uuid : cliente_uuid, payload: request.payload, error:error.sqlMessage});
				  reply({result:'KO',message:error.sqlMessage})
				})

		}

	}
}

exports.Query = function ( request, reply) {

	const Config = request.server.settings.app;
	const clienti = Config.clienti;
	const tabelle = Config.tabelle;

	var cliente_uuid = request.params.uuid_cliente;
  var collection_name = request.params.collection;

	var cliente = _.first(_.where(clienti, {uuid: cliente_uuid}));


	if ( cliente == null  ) {
		reply(Boom.notFound('cliente non presente'));
	} else {

			var table_name = "eventi_"+ cliente.idcliente +"_"+collection_name;

			var query = "SELECT * FROM "+table_name+"  WHERE "
			var where ="";
			if (request.query.evento_stato !=undefined)  {
				where =" evento_stato= " +request.query.evento_stato;
			}

			if (request.query.evento_agente_uuid!= undefined) {
				if (where !="" ) { where +=" AND ";}
				where +=" evento_agente_uuid= " +request.query.evento_agente_uuid;
			}

			if (request.query.evento_timestamp_min !=undefined && request.query.evento_timestamp_max!=undefined) {
				if (where !="" ) { where +=" AND ";}
				where +=" evento_timestamp BETWEEN '" +request.query.evento_timestamp_min +"' AND '" +request.query.evento_timestamp_max +"'";
			}

			if (request.query.evento_data_min !=undefined && request.query.evento_data_max!=undefined) {
				if (where !="" ) { where +=" AND ";}
				where +=" evento_data BETWEEN '" +request.query.evento_data_min +"' AND '" +request.query.evento_data_max +"'";
			}

			if ( where == "" ) {
				var errorBoom = Boom.create(400, "E\' necessario indicare almeno una condizione");
				reply(errorBoom);
			} else {
				query += where ;
				query +=" ORDER BY "+ request.query.sort + " LIMIT "+ request.params.limit;
				console.log(query)
				request.server.settings.app.knexEventi.raw(query, []).then(function(model) {
					reply(model[0]);
				}).catch(function(error) {
						 console.log(error);
						 var spError = { error : error[0] };
						 var errorBoom = Boom.create(400,  error.code, spError);
						 reply(errorBoom);
					});
			}



	}
}




exports.Event  = function ( request,reply) {

	const Config = request.server.settings.app;
	const clienti = Config.clienti;
	const tabelle = Config.tabelle;


	var cliente_uuid = request.params.uuid_cliente;
	var collection_name = request.params.collection;
  var evento_uuid = request.params.evento_uuid;

	var cliente = _.first(_.where(clienti, {uuid: cliente_uuid}));


	if ( cliente == null  ) {
		reply(Boom.notFound('cliente non presente'));
	} else {

		var table_name = "eventi_"+ cliente.idcliente +"_"+collection_name;
		var query = "SELECT * FROM "+table_name+"  WHERE evento_uuid='"+evento_uuid+"';"


		request.server.settings.app.knexEventi.raw(query, []).then(function(model) {
			reply(model[0][0]);
		}).catch(function(error) {
				 console.log(error);
				 var spError = { error : error[0] };
				 var errorBoom = Boom.create(400,  error.code, spError);
				 reply(errorBoom);
			});



	}



}
