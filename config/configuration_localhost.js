module.exports = {
  hapi : {
	  connection: {
        host: 'localhost',
        port: 3004,
        routes: {

                cors: {
                    origin: ["*"],
                    credentials: false
                }
            }
    },

	options : {
	  }
  },
  pino: {
	   name: 'salesrun-api',
	    slowtime:true
  },
  swagger :  {
        'info': {
            'title': 'SalesRun API Documentation',
            'version': '0.1.0'
        }
    },
  mysql :{
		salesrun: {
          host: '127.0.0.1',
          user: 'root',
          password: 'cavallo',
          database: 'salesrun'
        },
		eventi: {
          host: '127.0.0.1',
          user: 'root',
          password: 'cavallo',
          database: 'salesrun_collection'
        }
  }


};
