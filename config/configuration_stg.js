module.exports = {
  hapi : {
	  connection: {
        host: '127.0.0.1',
        port: 3004,
        routes: {
            cors: {
                origin: ["*"],
                credentials: false
            }
        }
    },

	options : {
	  }
  },
  pino: {
	   name: 'salesrun-api',
	    slowtime:true
  },
  swagger :  {
        'info': {
            'title': 'SalesRun API Documentation',
            'version': '0.1.0'
        },
        'host': '178.23.8.14:81'
    },
  mysql :{
		salesrun: {
          host: '127.0.0.1',
          user: 'root',
          password: 'cavallo1967.',
          database: 'salesrun2'
        },
		eventi: {
          host: '127.0.0.1',
          user: 'root',
          password: 'cavallo1967.',
          database: 'salesrun_collection'
        }
  }


};
