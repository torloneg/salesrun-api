var Api = require('../handlers/api');
var Joi = require('joi');

module.exports = [

  {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'static',
                redirectToSlash: true,
                index: true,
            }
        }
    },
    {
        method: 'POST',
        path: '/api/eventi/{uuid_cliente}/{collection}',
        config: {
            handler: Api.addEvent,
            tags: ['api','eventi'],
            validate : {
          				params : {
          					uuid_cliente : Joi.string().guid().required().description('Identificativo del cliente'),
          					collection : Joi.string().required().description('nome della collezione')
          				},
                  payload :  {
                        evento_data : Joi.string().regex(/20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(T|\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])/).required().description('data dell\'evento nel formato  YYYY-MM-DD hh:mm:ss'),
              					evento_agente_uuid : Joi.string().required().description('Identificativo del giocatore'),
              					evento_uuid : Joi.string().required().description('Identificativo del\'evento sul sistema esterno'),
              					fields : Joi.object().required().description('Oggetto json chiave:valore con i valori dell\'evento')
                  }
              }
        }
    },
    {
        method: 'GET',
        path: '/api/eventi/{uuid_cliente}/{collection}/{evento_uuid}',
        config: {
            handler: Api.Event,
            tags: ['api','eventi'],
            validate : {
                  params : {
                    uuid_cliente : Joi.string().guid().required().description('Identificativo del cliente'),
                    collection : Joi.string().required().description('nome della collezione'),
                    evento_uuid : Joi.string().required().description('Identificativo del\'evento sul sistema esterno')
                  }
              }
        }
    },
    {
        method: 'GET',
        path: '/api/eventi/query/{uuid_cliente}/{collection}/{start}/{limit}',
        config: {
            handler: Api.Query,
            tags: ['api','eventi'],
            validate : {
                  params : {
                    uuid_cliente : Joi.string().guid().required().description('Identificativo del cliente'),
                    collection : Joi.string().required().description('nome della collezione'),
                    start : Joi.number().required().default(0).description('primo record'),
                    limit : Joi.number().required().default(10).description('numero di record')
                  },
                  query : {
                    evento_stato: Joi.number().description('indicato se l\'evento è stato elaborato'),
                    evento_agente_uuid: Joi.string().description('identificativo univoco del giocatore'),
                    evento_timestamp_min: Joi.string().regex(/20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(T|\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])/).description('data min di inserimento dell\'evento nel formato  YYYY-MM-DD hh:mm:ss'),
                    evento_timestamp_max: Joi.string().regex(/20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(T|\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])/).description('data max di inserimento dell\'evento nel formato  YYYY-MM-DD hh:mm:ss'),
                    evento_data_min: Joi.string().regex(/20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(T|\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])/).description('data min dell\'evento nel formato  YYYY-MM-DD hh:mm:ss'),
                    evento_data_max: Joi.string().regex(/20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(T|\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])/).description('data max dell\'evento nel formato  YYYY-MM-DD hh:mm:ss'),
                    sort: Joi.string().default('evento_data desc').description('campo per l\'ordinamento asc|desc'),
                  }
              }
        }
    }



];
