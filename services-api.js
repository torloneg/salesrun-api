const Hapi = require('hapi');
const colors = require('colors2');
const Inert = require('inert');
const Vision = require('vision');

const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Pino = require('hapi-pino');
const Path = require('path');
const fs = require('fs');
const async = require('async');
const _ = require('underscore');


var env = '';
if (!fs.existsSync('./env')) {
    console.log('ERRORE !'.red);
    console.log('Il file di ambiente '.yellow + 'env'.green + ' non è presente sulla root del servizio.'.yellow);
    console.log('Il file deve contenere '.yellow + 'dev|prod'.red + ' da utilizzare per caricare il file presente nella cartella config.'.yellow);
    process.exit(1);
}

env = fs.readFileSync('./env')
console.log('ENV: '.green + env);

 var configPath = './config/configuration_'+ env;
  if (!fs.existsSync(configPath + ".js")) {
      console.log('ERRORE !'.red);
      console.log('Il file con la configurazione dell\'ambiente '.yellow + configPath+ ".js".green + ' non � presente nella cartella config.'.yellow);
      process.exit(1);
  }


var Config = require(configPath);




var knexEventi = require('knex')({
  client: 'mysql',
  connection: Config.mysql.eventi
});

var knexDB = require('knex')({
  client: 'mysql',
  connection: Config.mysql.salesrun
});


const server = new Hapi.Server(Config.hapi.options);





server.connection(Config.hapi.connection);

	const options = Config.swagger;
  console.log("swagger :".yellow,JSON.stringify(options))

	server.register([
		Inert,
		Vision,
		{
			'register': HapiSwagger,
			'options': options
		}], (err) => {

		});




	server.register({
      register: Pino.register,
      options: Config.pino
      }, (err) => {
		  if (err) {
			console.error(err)
			process.exit(1)
		  }

	  // the logger is available in server.app
	  server.app.logger.warn('Pino is registered')

	  // also as a decorated API
	  server.logger().info('another way for accessing it')

	  // and through Hapi standard logging system
	  server.log(['subsystem'], 'third way for accessing it')


	})



	server.route([

	]);

	server.route(Array.prototype.concat.apply([], require('./routes/api')));

	var table   = {};
	var clienti = {};

	async.waterfall([
		function(callback) {
			// LISTA DEI CLIENTI DISPONIBILI E ATTIVI SU SALESRUN

			knexDB.select('idcliente', 'uuid', 'nome').from('clienti').where('stato', 1).then(function(rows) {
				//console.log(JSON.stringify(rows))
				server.log(['debug'], {rows: rows})
				clienti = rows;

				callback(null, clienti, table);
			});


		},
		function(arg1, arg2, callback) {

			knexEventi.select('table_name','column_name','DATA_TYPE','IS_NULLABLE').from('information_schema.columns').where('table_name', "like", 'eventi_%').andWhere('table_schema', '=', 'salesrun_collection').then(function(rows) {
					table= rows;

					// console.log(JSON.stringify(rows).green)
					callback(null, table);
				})



		},
		function(arg1, callback) {

		//	console.log(table)

			callback(null, 'done');
		}
	], function (err, result) {

		var global = {
					env : env.toString(),
					config : Config,
					clienti : clienti,
					tabelle: table,
					knexEventi : knexEventi,
					knexDB : knexDB
				};


	   // console.log(JSON.stringify(global).yellow)

		server.settings.app = global;


		server.settings.app = global;

		server.start((err) => {

			if (err) {
				throw err;
			}




			console.log('Server running at: '.white + server.info.uri.green);
		});
	});
